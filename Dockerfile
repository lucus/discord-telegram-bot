FROM node:12
WORKDIR /usr/src/app

RUN apt-get update
RUN npm install -g forever
