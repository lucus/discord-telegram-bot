const Discord = require('discord.js');
const Moment = require('moment-timezone');
const discordTelegramChannelId = process.env.DISCORD_TELEGRAM_CHANNEL_ID;
const discordClient = new Discord.Client();
const discordBotToken = process.env.DISCORD_BOT_TOKEN;
const TelegramBotApi = require('telegram-bot-api');
const telegramBotToken = process.env.TELEGRAM_BOT_TOKEN;
let discordChannel = null;

const telegramBotApi = new TelegramBotApi({
    token: telegramBotToken
});
async function bootTelegramApi() {
    try {
        telegramBotApi.setMessageProvider(new TelegramBotApi.GetUpdateMessageProvider());
        await telegramBotApi.start();
        telegramBotApi.on('update', async (update) => {
            try {
                const date = Moment(update.message.date * 1000);
                const dateFormatted = date.tz('Europe/Amsterdam').format('DD-MM-YYYY HH:mm:ss');
                let name = update.message.from.first_name;
                if (typeof update.message.from.last_name !== 'undefined') {
                    name += ' ' + update.message.from.last_name;
                }
                if (typeof update.message.text === 'undefined') {
                    await discordChannel.send(dateFormatted + ' **' + name + '** said something, but I am currently not smart enough to show you what exactly, please don\'t hit me...');
                } else {
                    await discordChannel.send(dateFormatted + ' **' + name + '**: ' + update.message.text);
                }
            } catch (error) {
                logMessageWithTimestamp('Error handling telegram event', error);
            }
        });
        logMessageWithTimestamp('Successfully booted Telegram API');
    } catch (error) {
        logMessageWithTimestamp('Error while booting telegram API', error);
    }
}

async function bootDiscordApi() {
    try {
        await discordClient.login(discordBotToken);
        discordChannel = await discordClient.channels.fetch(discordTelegramChannelId);
        logMessageWithTimestamp('Successfully booted Discord API');
    } catch (error) {
        logMessageWithTimestamp('Error while booting discord API', error);
    }
}

async function boot() {
    await bootTelegramApi();
    await bootDiscordApi();
    logMessageWithTimestamp('All ready, listening for telegram updates...');
}

try {
    logMessageWithTimestamp('Booting...');
    boot();
} catch (error) {
    logMessageWithTimestamp('Error while booting', error);
}

function logMessageWithTimestamp(message) {
    const now = Moment.utc().format('DD-MM-YYYY HH:mm:ss');
    console.log(now + '(utc) : ' + message);
}
